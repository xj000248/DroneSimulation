
import java.util.Random;
import java.util.ArrayList;
/*
 * This is the arena of the drones
 * 
 * @author Stavros Efthymiou
 * 
 */
public class DroneArena {
	/*
	 * the constructor has the random generator to randomize the drones coordinates
	 * and has the arraylist
	 */
	Random randomGenerator;
	public int sizeX, sizeY;
	Drone d;
	ArrayList<Drone> drones;// arraylist named drones
/*
 * the get functions return attributes of the arena 
 */
	public int getX() {

		return sizeX;
	}

	public int getY() {

		return sizeY;
	}

	public void newArena() {
		drones.clear();
		drones = new ArrayList<Drone>();
	}

	public DroneArena(int x, int y) {// declaration
		sizeX = x;
		sizeY = y;
		randomGenerator = new Random();
		drones = new ArrayList<Drone>();
	}
/*
 * checks if there is drone in the position it is given and then displays  error if the arena is full
 * or if there is another drone in that position
 * 
 */
	public void addDrone() {
		int x, y;// declared out of the for loop

		if (drones.size() < sizeX * sizeY) {
			do {// find random numbers in the arena
				x = randomGenerator.nextInt(sizeX);// gets the value of the arena size and find a random number
				y = randomGenerator.nextInt(sizeY);
			} while (getDroneAt(x, y) != null);// while getDroneAt checks if any other drone is at that position

			Direction direction = Direction.randomDirection();
			d = new Drone(x, y, direction);// declaration of drone x and y position
			drones.add(d);// add the drone in the arraylist

		} else if (!drones.isEmpty()) {
			System.err.println("\nThe arena is already full!\n");
		} else {
			System.err.println("\nThe arena does not exist");
		}

	}
/*
 * checks if drone can move in the coordinates given
 * 
 */
	public boolean canMoveHere(int x, int y) {
		if (getDroneAt(x, y) != null || x >= sizeX || y >= sizeY || x < 0 || y < 0)
			return false;

		return true;
	}
/*
 * displays the information from the size of the arena given 
 * 
 */
	public String toString() {// gets the info requested in a sentence
		String s = "The arena size is " + sizeX + " x " + sizeY + " and: " + "\n";

		for (Drone d : drones) {// for all the drones inside the arraylist have them in the string
			s += d.toString() + "\n";
		}
		return s;
	}
/*
 * Displays drones in canvas
 * 
 */
	public void showDrones(ConsoleCanvas c) {
		for (Drone d : drones) {
			d.displayDrone(c);
		}

		// << loop through all the Drones calling the displayDrone method >>
	}
/*
 * checks if there is another drone in that position
 * 
 * 
 */
	public Drone getDroneAt(int x, int y) {
		Drone a = null;
		for (Drone d : drones) {// for the list of drones in the array
			if (d.isHere(x, y) == true) {// check if isHere is true(if sx and sy exist)
				return a = d;// then return d meaning position id clear put drone in array
			} else {
				return a;// otherwise null
			}
		}
		return a;// for the for loop return null if loop breaks

	}
/*
 * shows all drones in the interface
 * 
 * 
 */
	public void moveAllDrones() {
		for (Drone d : drones) {
			d.tryToMove(this);
		}

	}
}
/*
 * public static void main(String[] args) { DroneArena a = new DroneArena(20,
 * 10); // create drone arena a.addDrone();// get drone from addDrone
 * a.addDrone(); System.out.println(a.toString()); // print the string }
 * 
 * }
 */
