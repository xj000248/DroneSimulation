
public class Drone {
	private int droneX, droneY, droneID;
	private static int nextID = 1;
	private Direction dir;

	Drone(int x, int y, Direction d) { // declaration
		droneX = x;
		droneY = y;
		droneID = nextID;
		nextID = nextID + 1;
		dir = d;
	}

	public int getX() {

		return droneX;
	}

	public int getY() {

		return droneY;
	}

	public Direction getDir() {
		return dir;
	}

	public boolean isHere(int sx, int sy) {// returns true or false
//		<< write code here to return true or false >>		

		if (droneX == sx && droneY == sy) {// if the values droneX, droneY(position of drone) is the same as other x,y
			return true;
		} else {
			return false;
		}
	}

	public void displayDrone(ConsoleCanvas c) {
		char droneChr = 'D';
		c.showIt(droneX, droneY, droneChr);
		// << call the showIt method in c to put a D where the drone is
	}

	public void tryToMove(DroneArena a) {
		switch (dir) {
		case north:
			if (a.canMoveHere(droneX - 1, droneY)) // checks move eligibility
				droneX = droneX - 1; // drone moves
			else
				dir = dir.nextDirection(); // changes direction
			break;
		case east:
			if (a.canMoveHere(droneX, droneY + 1))
				droneY = droneY + 1;
			else
				dir = dir.nextDirection();
			break;
		case south:
			if (a.canMoveHere(droneX + 1, droneY))
				droneX = droneX + 1;
			else
				dir = dir.nextDirection();
			break;
		case west:
			if (a.canMoveHere(droneX, droneY - 1))
				droneY = droneY - 1;
			else
				dir = dir.nextDirection();
			break;
		default:
			break;
		}
	}

	public String toString() {// "collects" the info we want in a sentence and puts it as a string
		return "Drone " + droneID + " is at " + droneX + ", " + droneY + " with direction " + dir.toString();
	}
}
/*
 * public static void main(String[] args) { Drone z = new Drone(5, 3,
 * Direction.south); // create drone Drone w = new Drone(3, 5, Direction.south);
 * 
 * System.out.println(z.toString());// prints our string we created
 * System.out.println(w.toString());
 * 
 * } }
 */