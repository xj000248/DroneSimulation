import java.util.Random;
/*
 * Displays the directions the drones will follow
 * with the random  generator in south, west,north and east
 * 
 * @author Stavros Efthymiou
 */
public enum Direction {
	south, west, north, east;
/*
 * gets a random direction out of the four options
 */
	public static Direction randomDirection() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
/*checks if current direction is ordinal then faces it back 
 * otherwise it gets the next direction
 * 
 */
	public Direction nextDirection() {
		if (this.ordinal() == 3)
			return values()[0];
		else
			return values()[this.ordinal() + 1];
	}
}
/*
 * public static void main(String[] args) {
 * 
 * Direction dir = Direction.randomDirection();
 * System.out.println(dir.toString()); System.out.println(dir.nextDirection());
 * 
 * } }
 */