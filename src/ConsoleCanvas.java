
public class ConsoleCanvas {
	private char[][] canvas;
	private int canvasX;
	private int canvasY;

	/*
	 * This is the console canvas meaning the arena of the drones where they move
	 * inside
	 * 
	 * @author Stavros Efthymiou
	 * 
	 */
	public ConsoleCanvas(int arrayX, int arrayY) {
		canvasX = arrayX;
		canvasY = arrayY;
		canvas = new char[arrayX][arrayY];
		/*
		 * Displays appropriate characters in correct positions meaning # as barriers
		 * and spaces for the inside of the arena for the drones to move
		 * 
		 */

		for (int i = 0; i < canvasX; i++) {
			for (int j = 0; j < canvasY; j++) {
				canvas[i][j] = ' ';
				if (i == 0) {
					canvas[i][j] = '#';
				}
				if (j == 0) {
					canvas[i][j] = '#';
				}
				if (j == arrayY - 1) {
					canvas[i][j] = '#';
				}
				if (i == arrayX - 1) {
					canvas[i][j] = '#';
				}

			}
		}

	}

	/*
	 * The drones X and Y most be added by one so they do not spawn inside the barrier 
	 * 
	 */
	public void showIt(int droneX, int droneY, char ch) {

		canvas[droneX + 1][droneY + 1] = ch;//otherwise the drones would be displayed inside barrier
	}
/*
 * shows canvas information to string form 
 * 
 */
	public String toString() {
		String res = "";
		for (int i = 0; i < canvasX; i++) {
			for (int j = 0; j < canvasY; j++) {
				res = res + canvas[i][j] + " ";
			}
			res = res + "\n";
		}
		return res;
	}
}
/*
 * public static void main(String[] args) { ConsoleCanvas c = new
 * ConsoleCanvas(10, 5); // create a canvas c.showIt(4, 3, 'D'); // add a Drone
 * at 4,3 System.out.println(c.toString()); // display result }
 * 
 * }
 */
